import * as anchor from "@coral-xyz/anchor";
import { BN, Program } from "@coral-xyz/anchor";
import { utf8 } from "@coral-xyz/anchor/dist/cjs/utils/bytes";
import { assert } from "chai";
import { Lottery } from "../target/types/lottery";
import { LAMPORTS_PER_SOL } from "@solana/web3.js";
import * as fs from "fs";

describe("solana-lottery", () => {
  // Configure the client to use the local cluster.
  anchor.setProvider(anchor.AnchorProvider.env());

  const program = anchor.workspace.Lottery as Program<Lottery>;
  const publicKey = anchor.AnchorProvider.local().wallet.publicKey;
  console.log(publicKey);
  const wallet = anchor.web3.Keypair.fromSecretKey(
    Uint8Array.from(JSON.parse(fs.readFileSync("./wallets/id.json").toString()))
  ).publicKey;

  // it("Is Master initialized!", async () => {
  //   // Add your test here.
  //   const masterPda = await getMasterPDA();
  //   console.log(masterPda);
  //   await program.methods
  //     .initMaster()
  //     .accounts({
  //       master: masterPda,
  //       payer: publicKey,
  //       systemProgram: anchor.web3.SystemProgram.programId,
  //     })
  //     .rpc();

  //   const masterAccount = await program.account.master.fetch(masterPda);

  //   // console.log("Your transaction signature", tx);
  //   console.log(masterAccount);
  //   assert.equal(masterAccount.lastId, 0);
  // });

  it("Is Lottery Created!", async () => {
    const masterPda = await getMasterPDA();
    const masterAccount = await program.account.master.fetch(masterPda);

    const lotteryPda = await getLotteryPDA(masterAccount.lastId + 1);
    const ticketPrice = new BN(1).mul(new BN(LAMPORTS_PER_SOL));

    try {
      await program.methods
        .createLottery(ticketPrice)
        .accounts({
          lottery: lotteryPda,
          master: masterPda,
          authority: publicKey,
          systemProgram: anchor.web3.SystemProgram.programId,
        })
        .rpc();
    } catch (err) {
      console.log(err);
    }

    let lotteryAccount = await program.account.lottery.fetch(lotteryPda);

    console.log(lotteryAccount);

    assert.equal(lotteryAccount.id, 4);
    assert.equal(lotteryAccount.ticketPrice.toNumber(), 2000000000);
  });

  // it("Is Ticket Buy!!", async () => {
  //   // const masterPda = await getMasterPDA();
  //   // const masterAccount = await program.account.master.fetch(masterPda);

  //   const lotteryPda = await getLotteryPDA(1);
  //   const lotteryAccount = await program.account.lottery.fetch(lotteryPda);
  //   console.log(lotteryAccount);

  //   const ticketPda = await getTicketPDA(
  //     lotteryAccount.lastTicketId + 1,
  //     lotteryPda
  //   );
  //   try {
  //     await program.methods
  //       .buyTicket(lotteryAccount.id)
  //       .accounts({
  //         lottery: lotteryPda,
  //         ticket: ticketPda,
  //         // buyer: wallet,
  //         buyer: publicKey,
  //         systemProgram: anchor.web3.SystemProgram.programId,
  //       })
  //       .rpc();
  //   } catch (err) {
  //     console.log(err);
  //   }

  //   let ticketAccount = await program.account.ticket.fetch(ticketPda);
  //   console.log(ticketAccount);
  //   assert.equal(ticketAccount.id, 3);
  //   // assert.equal(ticketAccount.authority, publicKey);
  // });

  // it("Is winner Picked!!", async () => {
  //   const lotteryPDA = await getLotteryPDA(3);

  //   try {
  //     await program.methods
  //       .pickWinner(3)
  //       .accounts({
  //         lottery: lotteryPDA,
  //         authority: publicKey,
  //       })
  //       .rpc();
  //   } catch (err) {
  //     console.log(err);
  //   }

  //   const lotteryAccount = await program.account.lottery.fetch(lotteryPDA);
  //   console.log(lotteryAccount);

  //   assert.isNotNull(lotteryAccount.winnerId);
  // });

  // it("Is Prize Claimed!!", async () => {
  //   const lotteryPda = await getLotteryPDA(3);
  //   const ticketPda = await getTicketPDA(3, lotteryPda);

  //   try {
  //     await program.methods
  //       .claimPrize(3, 1)
  //       .accounts({
  //         lottery: lotteryPda,
  //         ticket: ticketPda,
  //         authority: publicKey,
  //         systemProgram: anchor.web3.SystemProgram.programId,
  //       })
  //       .rpc();
  //   } catch (err) { console.log(err) }

  //   let winnerTicket = await program.account.ticket.fetch(ticketPda);
  //   let lotteryAccount = await program.account.lottery.fetch(lotteryPda);
  //   console.log(lotteryAccount);
  //   console.log(winnerTicket);
  //   assert.equal(lotteryAccount.claimed, true);
  // });

  async function getMasterPDA(): Promise<anchor.web3.PublicKey> {
    const [masterPDA] = anchor.web3.PublicKey.findProgramAddressSync(
      [utf8.encode("master")],
      program.programId
    );
    return masterPDA;
  }

  async function getLotteryPDA(lastid: number): Promise<anchor.web3.PublicKey> {
    const lastIdBuffer = new BN(lastid).toBuffer("le", 4);
    console.log(lastIdBuffer);
    const [lotteryPDA] = anchor.web3.PublicKey.findProgramAddressSync(
      [utf8.encode("lottery"), lastIdBuffer],
      program.programId
    );

    return lotteryPDA;
  }

  async function getTicketPDA(
    ticket_id: number,
    lotteryKey: anchor.web3.PublicKey
  ): Promise<anchor.web3.PublicKey> {
    const [ticketPDA] = anchor.web3.PublicKey.findProgramAddressSync(
      [
        utf8.encode("ticket"),
        lotteryKey.toBuffer(),
        new BN(ticket_id).toBuffer("le", 4),
      ],
      program.programId
    );
    return ticketPDA;
  }
});
