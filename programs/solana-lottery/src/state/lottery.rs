use anchor_lang::{
    prelude::*,
    solana_program::{clock::Clock, hash::hash},
};

#[account]
#[derive(InitSpace)]
pub struct Lottery {
    pub id: u32,                // 4
    pub authority: Pubkey,      // 32
    pub ticket_price: u64,      //8
    pub last_ticket_id: u32,    // 4
    pub winner_id: Option<u32>, // 4 + 1
    pub claimed: bool,          //1
}

impl Lottery {
    pub fn get_winner_id(&self) -> Result<u32> {
        let clock = Clock::get()?;
        let pseudo_random_number: u32 = (u64::from_le_bytes(
            <[u8; 8]>::try_from(&hash(&clock.unix_timestamp.to_le_bytes()).to_bytes()[..8])
                .unwrap(),
        ) as u128
            * clock.slot as u128) as u32
            % u32::MAX;
        let winner_id: u32 = (pseudo_random_number % self.last_ticket_id) + 1;

        Ok(winner_id)
    }
}
