use anchor_lang::prelude::error_code;

#[error_code]
pub enum LotteryError {
    #[msg("winner already exists")]
    WinnerAlreadyExists,

    #[msg("Can't choose a winner when there are no tickets.")]
    NoTickets,

    #[msg("Winner han not been choosen yet.")]
    WinnerNotChosen,

    #[msg(Invalid winner.)]
    InvalidWinner,

    #[msg("The prize has already been claimed")]
    AlreadyClaimed,
}
