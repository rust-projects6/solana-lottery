use crate::constants::MASTER_SEED;
use crate::state::master::*;
use anchor_lang::prelude::*;

#[derive(Accounts)]
pub struct InitMaster<'info> {
    #[account(init, payer=payer, space = 4 + 8, seeds=[MASTER_SEED.as_bytes()], bump)]
    pub master: Account<'info, Master>,
    #[account(mut)]
    pub payer: Signer<'info>,

    pub system_program: Program<'info, System>,
}
