use anchor_lang::prelude::*;

use crate::{
    constants::{LOTTERY_SEED, TICKET_SEED},
    state::{lottery::Lottery, ticket::Ticket},
};

#[derive(Accounts)]
#[instruction(lottery_id: u32, ticket_id: u32)]
pub struct ClaimPrize<'info> {
    #[account(mut, seeds = [LOTTERY_SEED.as_bytes(), lottery_id.to_le_bytes().as_ref()], bump)]
    pub lottery: Account<'info, Lottery>,

    #[account(seeds = [TICKET_SEED.as_bytes(), lottery.key().as_ref(), ticket_id.to_le_bytes().as_ref()], bump, has_one=authority)]
    pub ticket: Account<'info, Ticket>,

    #[account(mut)]
    pub authority: Signer<'info>,

    pub system_program: Program<'info, System>,
}
