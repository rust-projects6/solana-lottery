use anchor_lang::prelude::*;

use crate::constants::*;
use crate::state::lottery::Lottery;

#[derive(Accounts)]
#[instruction(lottery_id: u32)]
pub struct PickWinner<'info> {
    #[account(mut, seeds=[LOTTERY_SEED.as_bytes(), &lottery_id.to_le_bytes()], bump, has_one = authority)]
    pub lottery: Account<'info, Lottery>,

    #[account(mut)]
    pub authority: Signer<'info>,
}

// impl<'info> PickWinner<'info> {
//     pub fn get_winner_id(&self) -> Result<u32> {
//         let clock = Clock::get()?;
//         let pseudo_random_number: u32 = ((u64::from_le_bytes(
//             <[u8; 8]>::try_from(&hash(&clock.unix_timestamp.to_le_bytes()).to_bytes()[..8])
//                 .unwrap(),
//         ) * clock.slot)
//             % u32::MAX as u64) as u32;
//         let winner_id: u32 = (pseudo_random_number % self.lottery.last_ticket_id) + 1;

//         Ok(winner_id)
//     }
// }
