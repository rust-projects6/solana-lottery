use anchor_lang::prelude::*;

use crate::constants::*;
use crate::state::lottery::Lottery;
use crate::state::ticket::Ticket;

#[derive(Accounts)]
#[instruction(lottery_id:u32)]
pub struct BuyTicker<'info> {
    #[account(mut, seeds=[LOTTERY_SEED.as_bytes(), lottery_id.to_le_bytes().as_ref()], bump)]
    pub lottery: Account<'info, Lottery>,

    #[account(init, payer=buyer, space = 8 + Ticket::INIT_SPACE, seeds = [TICKET_SEED.as_bytes(), lottery.key().as_ref(), (lottery.last_ticket_id+1).to_le_bytes().as_ref()], bump)]
    pub ticket: Account<'info, Ticket>,

    #[account(mut)]
    pub buyer: Signer<'info>,

    pub system_program: Program<'info, System>,
}
