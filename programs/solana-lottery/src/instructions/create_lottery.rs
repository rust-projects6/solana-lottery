use anchor_lang::prelude::*;

use crate::{
    constants::{LOTTERY_SEED, MASTER_SEED},
    state::{lottery::Lottery, master::Master},
};

#[derive(Accounts)]
pub struct CreateLottery<'info> {
    #[account(init, payer = authority, seeds=[LOTTERY_SEED.as_bytes(), &(master.last_id + 1).to_le_bytes()], bump, space = 8 + Lottery::INIT_SPACE)]
    pub lottery: Account<'info, Lottery>,

    #[account(mut, seeds = [MASTER_SEED.as_bytes()], bump)]
    pub master: Account<'info, Master>,

    #[account(mut)]
    pub authority: Signer<'info>,
    pub system_program: Program<'info, System>,
}
